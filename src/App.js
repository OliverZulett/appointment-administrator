import {Fragment, useState, useEffect} from "react";
import Appointment from "./components/Appointment";
import Form from "./components/form";
import PropTypes from "prop-types";

function App() {
  let appointmentsOnLocalStorage = JSON.parse(localStorage.getItem('appointments'));
  if(!appointmentsOnLocalStorage) {
    appointmentsOnLocalStorage = [];
  }

  // appintments array
  const [appointments, saveAppointments] = useState(appointmentsOnLocalStorage);

  // useEffect to store data in localStorage
  // useEffect se ejecuta cada vez que cambia el array appointments
  useEffect(() => {
    localStorage.setItem('appointments', JSON.stringify(appointments));
  }, [appointments]);

  const createAppointment = appointment => saveAppointments([...appointments,appointment])

  const removeAppointment = id => {
    const newAppointments = appointments.filter(appointment => appointment.id !== id);
    saveAppointments(newAppointments);
  };

  return (
    <Fragment>
      <h1>Medical Dates Administrator</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Form
              createAppointment={createAppointment}
            />
          </div>
          <div className="one-half column">
            {
              appointments.length > 0
              ? <h2>Manage your appointments</h2>
              : <h2>No appointments</h2>
            }
            {appointments.map(appointment => (
              <Appointment
                key={appointment.id}
                appointment={appointment}
                removeAppointment={removeAppointment}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

Form.propTypes = {
  createAppointment: PropTypes.func.isRequired
}

export default App;
