import {Fragment, useState} from 'react';
import { v4 as uuidv4 } from 'uuid';

const Form = ({createAppointment}) => {
  // create State for appointments
  const [appointment, updateAppointment] = useState({
    pet: '',
    owner: '',
    date: '',
    time: '',
    symtoms: ''
  });

  const [error, updateError] = useState(false);

  // handle change
  const  handleChange = event => {
    console.log('writing...', event.target.name, event.target.value);
    updateAppointment({
      ...appointment,
      [event.target.name]: event.target.value
    })
  };

  // extract values
  const { pet, owner, date, time, symtoms } = appointment;

  // save appointment
  const submitAppointment = event => {
    event.preventDefault();
    console.log('sending form...');
    // validate
    if (pet.trim() === '' || owner.trim() === '' || date.trim() === '' || time.trim() === '' || symtoms.trim() === '') {
      console.log('pet error');
      updateError(true);
      return;
    }
    // remove previous message
    updateError(false);
    // assing an id
    appointment.id = uuidv4();
    // create appointment
    createAppointment(appointment);
    // reset form
    updateAppointment({
      pet: '',
      owner: '',
      date: '',
      time: '',
      symtoms: ''
    });
    console.log('adding...');
  };

  return (
    <Fragment>
      <h2>Create appointment</h2>
      {
        error 
        ? <p className="alerta-error">All fields are necessary</p>
        : null
      }
      <form
        onSubmit={submitAppointment}
      >
        <label>Pet name</label>
        <input
          type="text"
          name="pet"
          className="u-full-width"
          placeholder="pet name"
          onChange={handleChange}
          value={pet}
        />
        <label>Owner name</label>
        <input
          type="text"
          name="owner"
          className="u-full-width"
          placeholder="owner name"
          onChange={handleChange}
          value={owner}
        />
        <label>Date</label>
        <input
          type="date"
          name="date"
          className="u-full-width"
          onChange={handleChange}
          value={date}
        />
        <label>Time</label>
        <input
          type="time"
          name="time"
          className="u-full-width"
          onChange={handleChange}
          value={time}
        />
        <label>Symtoms</label>
        <textarea
          type="text"
          name="symtoms"
          className="u-full-width"
          placeholder="symtoms"
          onChange={handleChange}
          value={symtoms}
        ></textarea>
        <button
          type="submit"
          className="u-full-width button-primary"
        >Add Appointment</button>
      </form>
    </Fragment>
  );
}
 
export default Form;