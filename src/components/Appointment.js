const Appointment = ({appointment, removeAppointment}) => (
  <div className="cita">
    <p>Pet: <span>{appointment.pet}</span></p>
    <p>Owner: <span>{appointment.owner}</span></p>
    <p>Date: <span>{appointment.date}</span></p>
    <p>Time: <span>{appointment.time}</span></p>
    <p>Symtoms: <span>{appointment.symtoms}</span></p>
    <button
      className="button eliminar u-full-width"
      onClick={() => removeAppointment(appointment.id)}
    >Remove &times;</button>
  </div>
);
 
export default Appointment;